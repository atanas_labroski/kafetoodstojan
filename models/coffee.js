var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CoffeeSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    coffeeTime:{
        type: Date,
        required: "Required date OMG!"
    }
});

mongoose.model('Coffee', CoffeeSchema);