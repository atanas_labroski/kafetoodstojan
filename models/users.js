var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    shortname: {
        type: String,
        default: '',
        required: 'Задолжително си е батоооо.'
    },
    coffee: {
        type: Schema.ObjectId,
        ref: 'Coffee',
        required: "Без кафе каде?"
    }
});

mongoose.model('User', UserSchema);