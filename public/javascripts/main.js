/**
 * Created by Labroski on 10/8/2014.
 */

var coffeeApp = angular.module("coffeeApp", []);

coffeeApp.controller('coffeeController', function ($scope, $interval, coffeeService) {
    $scope.users = [];
    $scope.user = {};
    $scope.coffee = {};
    $scope.coffeeTime = "";
    $scope.waste = 0;
    $scope.timeToWait = "";
    $scope.timer = {
        h: 0,
        m: 0,
        s: 0,
        wholeTimeInMS: 0
    };

    function calculateWaste() {
        var wasteInt = Math.round(Math.pow(($scope.users.length * 18), 1.18));
        var wasteMin = Math.floor(wasteInt / 60);
        var wasteSec = wasteInt - wasteMin * 60;

        var wasteString = "";
        wasteString += wasteMin == 0 ? "" : "" + wasteMin + "m";
        wasteString += " " + wasteSec + "s";
        return wasteString;
    }


    $scope.setTheTimer = function() {
        var x = $scope.timer.wholeTimeInMS / 1000;
        var seconds =  Math.floor(x % 60);
        x /= 60;
        var minutes = Math.floor(x % 60);
        x /= 60;
        var hours =  Math.floor(x % 24);

        $scope.timer.s = seconds;
        $scope.timer.m = minutes;
        $scope.timer.h = hours;

    };

    $scope.joiner = function() {
        var $insertCoffeeDiv = $("#insertCoffeeDiv");
        if ($scope.user.shortname === "SSM") {
            $insertCoffeeDiv.slideDown();
        } else {
            if ($scope.user.shortname == undefined) {
                return;
            } else {
                for(var i = 0; i < $scope.users.length; i++) {
                    var insertedUser = $scope.users[i];
                    if (insertedUser.shortname.toUpperCase() == $scope.user.shortname.toUpperCase()) {
                        return;
                    }
                }

                coffeeService
                    .newJoiner($scope.user, $scope.coffee._id)
                    .then(
                    function(res) {
                        $scope.users.push({'shortname': res.shortname });
                        $scope.waste = calculateWaste();
                    },
                    function(error) {
                        console.log("then response: " + JSON.stringify(error));
                    });
            }
        }
    };

    $scope.newCoffee = function () {
        var date = new Date();
        var hmsString = "" + $scope.coffeeTime;
        var hms = hmsString.split(":");
        var minutes = parseInt(hms[1]);
        minutes = minutes.length == 1 ? "0" + minutes : minutes;
        date.setHours(parseInt(hms[0]));
        date.setMinutes(minutes);

        coffeeService
            .createCoffee(
                $scope.password,
                date)
            .then(
                function(res) {
                    console.log("then res" + JSON.stringify(res));
                    if (res._id != null) {
                        var $insertCoffeeDiv = $("#insertCoffeeDiv");
                        $insertCoffeeDiv.slideUp();
                        $scope.users = [];
                        $scope.waste = calculateWaste();

                        var date = res.coffeeTime;

                        var now = new Date();

                        if (date < now) {
                            $scope.coffeeTime = "W8 FOR 100le TO CREATE COFFEE EVENT";
                        } else {
                            var day = now.getDay() - date.getDay();
                            var hours = date.getHours() - now.getHours();

                            if (day != 0 && hours < 0) {
                                $scope.timeToWait = "Not Today";
                            } else {
                                var differenceInMS = Math.abs(date - now);
                                $scope.timer.wholeTimeInMS = differenceInMS;
                                $scope.setTheTimer();
                            }
                        }


                    }
                },
                function(error) {
                    console.log("then error" + JSON.stringify(error));
                }
            );
    };

    $scope.init = function() {
        $("#close").click(function() {
            var $insertCoffeeDiv = $("#insertCoffeeDiv");
            $insertCoffeeDiv.slideUp();
        });

        coffeeService.getLastCoffee().then(
            function(res) {
                $scope.coffee = res;
                var date = new Date($scope.coffee.coffeeTime);
                var now = new Date();
                if (date < now) {
                    alert("W8 FOR 100le TO CREATE COFFEE EVENT");
                    $scope.coffeeTime = "W8 FOR 100le TO CREATE COFFEE EVENT";
                } else {

                    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                    $scope.coffeeTime = date.getHours() + ":" + minutes;

                    console.log("coffee: " + JSON.stringify($scope.coffee));
                    coffeeService.getUsers($scope.coffee._id).then(
                        function(res) {
                            $scope.users = res;
                            $scope.waste = calculateWaste();
                        },
                        function(error) {
                            console.log("then error" + JSON.stringify(error));
                        }
                    );

                    var day = now.getDay() - date.getDay();
                    var hours = date.getHours() - now.getHours();

                    if (day != 0 && hours < 0) {
                        $scope.timeToWait = "Not Today";
                    } else {
                        var differenceInMS = Math.abs(date - now);
                        $scope.timer.wholeTimeInMS = differenceInMS;
                        $scope.setTheTimer();
                        $scope.startInterval();
                    }
                }

            },
            function(err) {
                console.log(err);
            }
        );

    };

    $scope.startInterval = function() {
        $interval(function(){
            $scope.timer.wholeTimeInMS -= 1000;
            $scope.setTheTimer();
        }, 1000);
    };

});

coffeeApp.factory("coffeeService", function($http, $q) {
    var factory = {};

    factory.createCoffee = function (password, time) {
        var defer = $q.defer();

        $http
            .post("/users/coffee", {password: password, "coffeeTime": time})
            .success(function (response) {
                console.log(response);
                defer.resolve(response);
            })
            .error(function(error) {
                console.log(error);
                defer.reject(error);
            });

        return defer.promise;
    };

    factory.newJoiner = function (user, coffeeId) {
        var defer = $q.defer();

        $http({
                url: "/users/new",
                method: "POST",
                data: {user: user, coffeeId: coffeeId}
            })
            .success(function (res) {
                console.log(res);
                defer.resolve(res);
            })
            .error(function(error) {
                console.log(error);
                defer.reject(error);
            });

        return defer.promise;
    };

    factory.getUsers = function(coffeeId) {
        var defer = $q.defer();

        $http.get("/users/all?coffeeId=" + coffeeId, {})
            .success(function(res) {
                defer.resolve(res);
            })
            .error(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    };

    factory.getLastCoffee = function() {
        var defer = $q.defer();

        $http.get("/users/getLastCoffee")
            .success(function(res) {
                defer.resolve(res);
            })
            .error(function(error) {
                defer.reject(error);
            });

        return defer.promise;
    };

    return factory;
});