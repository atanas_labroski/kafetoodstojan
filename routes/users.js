var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Coffee = mongoose.model('Coffee');

/**
 * inserts new user in the database for given coffee
 */
router.post('/new', function (req, res) {
    var user = new User(req.body.user);//shortname, coffee
    user.coffee = req.body.coffeeId;

    user.shortname = user.shortname.toUpperCase();
    user.save(function (err) {
        if (err) {
            res.send(err);
        } else {
            res.json(user);
        }
    });
});

/**
 * Creates new coffee.
 */
router.post('/coffee', function (req, res) {
    if(req.body.password != "pass"){
        res.json({message: 'You\'re not 100le!'});
    }
    else {
        var coffee = new Coffee({coffeeTime: req.body.coffeeTime});
        coffee.save(function (err) {
            if (err) {
                res.send(err);
            } else {
                res.json(coffee);
            }
        });
    }
});

/**
 * Gets the latest coffee.
 */
router.get('/getLastCoffee', function (req, res) {
    Coffee.findOne({}, {}, { sort: { 'created': -1 } }, function (err, coffee) {
        if (err) {
            res.send(err);
        } else {
            res.json(coffee);
        }
    });
});

/**
 * Gets all users for the latest coffee
 */
router.get('/all', function(req, res){
    User.find({coffee: req.param("coffeeId")}).sort('shortname').exec(function(err, users) {
        if (err) {
            res.send(err);
        } else {
            res.json(users);
        }
    });

});

/**
 * Gets all users for the latest coffee
 */
router.get('/allCoffees', function(req, res){
    Coffee.find().exec(function(err, coffees) {
        if (err) {
            res.send(err);
        } else {
            res.json(coffees);
        }
    });
});

module.exports = router;